package io.ipfs.format;

import androidx.annotation.NonNull;

public interface NodeAdder {
    void Add(@NonNull Node nd);
}
